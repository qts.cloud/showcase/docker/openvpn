# OpenVPN
[![semantic-release-badge]][semantic-release]
[![pipeline-badge]][pipeline-status]

## Setup OpenVPN

* Prequisites:
  * basic linux knowledge
  * have docker already installed
  * have full access to the machine where docker is installed. We will refer to it as `docker-vm` from now on.
  * be able to do port-forwarding to `docker-vm`
  * [vault container up and running](https://gitlab.com/qts.cloud/showcase/docker/vault)

* Setup:
  * connect to `docker-vm` machine and create your work directory

    ```sh
    ssh docker-vm
    # Preparing structure
    mkdir -p qts && cd qts
    git clone https://gitlab.com/qts.cloud/showcase/docker/openvpn.git
    cd openvpn
    ```

  * Check docker-compose.yml

    ```yaml
    version: "3"

    services:
      openvpn:
        image: ibacalu/openvpn:latest
        environment:
          VPN_ENV: dev
          VAULT_TOKEN: "${VAULT_TOKEN}"
          VAULT_ADDR: "${VAULT_ADDR}"
          VAULT_KV_PATH: openvpn
          # VPN_CNAME: "localhost"      # CNAME has priority.
          # PUBLIC_IP: "127.0.0.1"      # If not provided, the init script will try to determine it.
        restart: always
        network_mode: host
        ports:
          - 1195:1195
        cap_add:
          - NET_ADMIN
          - NET_RAW
        devices:
          - "/dev/net/tun"
    ```

  * Start Openvpn

    ```sh
    # Export Vault Config
    export VAULT_TOKEN='s.XXXXXXXXX'
    export VAUTL_ADDR='http://127.0.0.1:8200'

    # Launch container
    docker-compose up -d                # Run the container as a daemon
    docker-compose logs -f openvpn      # Follow the logs
    [...]
    openvpn_1  | Loading config from Vault
    openvpn_1  |       VAULT_ADDR:  http://vault.qts.cloud:8200
    openvpn_1  |       Config:      config
    openvpn_1  |       Environment: dev
    openvpn_1  | [INFO]: Configure iptables rules
    openvpn_1  | [INFO]: Configure OpenVPN certs
    openvpn_1  | [INFO]: Configure OpenVPN service
    openvpn_1  | [INFO]: Configure OpenVPN client
    openvpn_1  | [INFO]: Configure Login server
    openvpn_1  | [INFO]: Display generated client.ovpn
    openvpn_1  | # OpenVPN configuration below
    openvpn_1  | # Save it as config.ovpn
    openvpn_1  | # --------------------------------------------------------------
    openvpn_1  | client
    openvpn_1  | proto udp
    openvpn_1  | remote vpn.qts.cloud 1195
    openvpn_1  | 
    openvpn_1  | dev tun
    openvpn_1  | tls-client
    openvpn_1  | remote-cert-tls server
    openvpn_1  | auth-user-pass
    openvpn_1  | cipher AES-256-CBC
    openvpn_1  | 
    openvpn_1  | <ca>
    openvpn_1  | -----BEGIN CERTIFICATE-----
    openvpn_1  | MIIDPDCCAiSgAwIBAgIUV6Lv5advf3/R4Nsktc9/sLgOtmswDQYJKoZIhvcNAQEL
    openvpn_1  | BQAwETEPMA0GA1UEAwwGc2VydmVyMB4XDTIwMDgwMTE1MzQxOFoXDTIxMDgwMTE1
    openvpn_1  | MzQxOFowETEPMA0GA1UEAwwGc2VydmVyMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8A
    openvpn_1  | MIIBCgKCAQEAp2RF7sl7tCba3CDrajwMp9XIbeSnB/Exlfkx2OLpqzTaN2pX8GYi
    openvpn_1  | YWvY7qNQflM/NcXI93XbeCouRd4TMs5vIrFTgDg6HxgPpivTMSh2aivt4qAuSJst
    openvpn_1  | z0Qa42X9f4hnp1BGG5hDqoVCsHxSJP1xVb6wiZVcRqDHqGTojGJ9aQk92Wo4MDgp
    openvpn_1  | 0dDwxbqcovdRt3ldEa82zGrfMygTYx4caMueH7nN5hcPBtum2hCxoo4f2cPNnqKo
    openvpn_1  | UCK0AdnFZnRVLK2ks6US/cl5vQwcGvfyH78SsRNOOvrLp0myOjW2v300umNAB5jC
    openvpn_1  | GjLbonsITnBgx+/Z9pudqGaWNO7YvMNhEwIDAQABo4GLMIGIMB0GA1UdDgQWBBTj
    openvpn_1  | 6s73vtdaGC9y6Mdy/ojk6WcgtjBMBgNVHSMERTBDgBTj6s73vtdaGC9y6Mdy/ojk
    openvpn_1  | 6WcgtqEVpBMwETEPMA0GA1UEAwwGc2VydmVyghRXou/lp29/f9Hg2yS1z3+wuA62
    openvpn_1  | azAMBgNVHRMEBTADAQH/MAsGA1UdDwQEAwIBBjANBgkqhkiG9w0BAQsFAAOCAQEA
    openvpn_1  | pwAOgoJaMgS7uIB6VdQNAOHsbkpPSlIofViDb5tsxmx/8zh1tGRfUalvG1lzoeTy
    openvpn_1  | JA/kFyURigUU/cwOISFZIhmKaC60xdxH03TfHdkQusVY9LJNdPpLU1X7DxmJBQ5V
    openvpn_1  | e2uuyr88S0gbIQyZQt6/JsEmdPxwjfZmkHpinySZYky7ksHQNdHiVGtMcP+jUJUZ
    openvpn_1  | FBwV2HwZMxXsKR+DBU/KO4mFSS8cLCs1qbaTip37Q1xHLNLu4cRhKRA5ezH7yD16
    openvpn_1  | 8ONZe3lF9rcmoO8gFblQLfzeWXVVHYVvBGLLl+HORKZa0FoWUh/roWmA2Kk4cNcB
    openvpn_1  | yJiqRxPNVAf/DsLb6Fap6g==
    openvpn_1  | -----END CERTIFICATE-----
    openvpn_1  | 
    openvpn_1  | </ca>
    openvpn_1  | # --------------------------------------------------------------
    openvpn_1  | [INFO]: Starting OpenVPN
    [...]
    ```

  * At first boot, the container will generate required certificates and store the configuration in Vault.
  * You can see the client vpn config in the logs above. Use it to generate the config.ovpn
  * Create users
    * Connect and authenticate to vault.
    * If you have vault binary installed and configured locally you can run this from your own machine

      ```bash
      # This values should be same as above
      export VPN_ENV="dev"
      export VAULT_TOKEN: "<FILL-THIS>"
      export VAULT_ADDR: "http://<FILL-THIS>:8200"
      vault write auth/openvpn/users/john.doe \
                  password=My-Secret-Password \
                  policies=${VPN_ENV}
      ```

    * Otherwise, you can use Vault to create users
      * login to Vault UI (`VAULT_ADDR` mentioned in the config)
      * start the terminal (top-right)
      * update command below and run it (`policies=${VPN_ENV}`)

      ```sh
      write auth/openvpn/users/john.doe password="My-Secret-Password" policies=dev
      ```

    * Other cool commands:

    ```sh
    # List users
    list auth/openvpn/users

    # Get User info/policies. Policies are helpful with multiple VPN deployments
    read auth/openvpn/users/john.doe
    ```

    ---

[pipeline-status]: https://gitlab.com/qts.cloud/showcase/docker/openvpn/-/commits/master
[pipeline-badge]: https://gitlab.com/qts.cloud/showcase/docker/openvpn/badges/master/pipeline.svg
[semantic-release]: https://semantic-release.gitbook.io
[semantic-release-badge]: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg

