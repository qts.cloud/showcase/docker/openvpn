#!/bin/ash

export IP=$(ip addr | grep 'inet' | grep -v inet6 | grep -vE '127\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | grep -oE '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | head -1)
export DEFAULT_GW="$(ip route show default | awk '{print $5}')"

{{ with printf "%s/%s" (env "VAULT_PATH") (env "CONFIG") | secret }}
export TUN="{{ .Data.data.tun }}"

{{ .Data.data.iptables }}
{{ end }}