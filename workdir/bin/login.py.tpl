#!/usr/bin/env python

import sys
import os
import hvac

vault_addr = "{{ env "VAULT_ADDR" }}"
env = "{{ env "VPN_ENV" }}"

auth_user = os.environ.get('username', None)
auth_pass = os.environ.get('password', None)

client = hvac.Client(url=vault_addr)

def login(username, password):
    try:
        result = client.auth_userpass(username, password, mount_point='openvpn')
        if env in result['auth']['policies']:
            # Success!
            return True
        else:
            # User does not have required policy
            return False
    except hvac.exceptions.InvalidRequest as error:
        print(error)
        return False

if login(auth_user, auth_pass):
    print(f"Auth success for '{auth_user}' with policy '{env}'")
    sys.exit(0)
else:
    print(f"Auth failed for '{auth_user}' with policy '{env}'")
    sys.exit(1)
