{{ with printf "%s/server.crt" (env "VAULT_PATH") | secret }}
{{ .Data.data.value }}
{{ end }}
