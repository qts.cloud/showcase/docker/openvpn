{{ with printf "%s/dh2048.pem" (env "VAULT_PATH") | secret }}
{{ .Data.data.value }}
{{ end }}
