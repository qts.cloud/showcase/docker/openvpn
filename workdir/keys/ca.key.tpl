{{ with printf "%s/ca.key" (env "VAULT_PATH") | secret }}
{{ .Data.data.value }}
{{ end }}
