# OpenVPN configuration below
# Save it as config.ovpn
# --------------------------------------------------------------
client
{{ with printf "%s/%s" (env "VAULT_PATH") (env "CONFIG") | secret }}proto {{ .Data.data.protocol }}
{{ if eq (env "VPN_CNAME") "undefined" }}remote {{ .Data.data.public_ip }} {{ .Data.data.port }}
{{else}}remote {{ .Data.data.cname }} {{ .Data.data.port }}{{end}}
{{ end }}
dev tun
tls-client
remote-cert-tls server
auth-user-pass
cipher AES-256-CBC

<ca>
{{ with printf "%s/ca.crt" (env "VAULT_PATH") | secret }}{{ .Data.data.value }}{{ end }}
</ca>
# --------------------------------------------------------------
