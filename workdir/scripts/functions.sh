#!/bin/ash

function colorize () {
    export BLACK="\033[30m"
    export RED="\033[31m"
    export GREEN="\033[92m"
    export YELLOW="\033[33m"
    export BLUE="\033[34m"
    export MAGENTA="\033[35m"
    export CYAN="\033[36m"
    export WHITE="\033[37m"
    export VIVID="\033[95m"
    export RST="\033[0m"
}

function log_info {
    echo -e "${GREEN}[INFO]: $1${RST}"
}

function log_warning {
    echo -e "${YELLOW}[WARNING]: $1${RST}"
}

function log_error {
    echo -e "${RED}[ERROR]: $1${RST}"
}

function log_fatal {
    echo -e "${RED}[FATAL]: $1${RST}"
    exit 1
}

function check_variables_defined {
    for var in "$@"; do
        if [[ -z $(eval "echo \$$var") ]]; then log_error "${var} not defined!"; exit 1; fi
    done
}

function conf_iptables() {
    log_info "Configure iptables rules"
    consul-template -config ${WORKDIR}/consul.hcl -template "${WORKDIR}/bin/iptables.sh.tpl:${WORKDIR}/bin/iptables.sh" -once
    chmod +x ${WORKDIR}/bin/iptables.sh
    ${WORKDIR}/bin/iptables.sh
}

function vault_secrets_enable () {
    local VAULT_SECRET=$1
    local VAULT_PATH=${2:-$VAULT_SECRET}
    local VAULT_PARAMS=$3
    vault secrets enable -path=$VAULT_PATH ${VAULT_PARAMS} $VAULT_SECRET || true
}

function vault_secrets_disable () {
    local VAULT_PATH=$1
    vault secrets disable $VAULT_PATH || true
}

function vault_auth_enable () {
    local VAULT_SECRET=$1
    local VAULT_PATH=${2:-$VAULT_SECRET}
    local VAULT_PARAMS=$3
    vault auth enable -path=$VAULT_PATH ${VAULT_PARAMS} $VAULT_SECRET || true
}

function vault_auth_disable () {
    local VAULT_PATH=$1
    vault auth disable $VAULT_PATH || true
}

function check_vault_login () {
    log_info "Checking Vault Auth"
    vault login ${VAULT_TOKEN} &> /dev/null 
    if [[ $? != 0 ]]; then log_fatal "Could not login to Vault"; exit 1; fi
}

function check_vault_key_exists () {
    log_info "Checking VPN Configuration"
    for var in "$@"; do
        vault kv get $var 1> /dev/null 
        if [[ $? != 0 ]]; then log_warning "${var} not found"; export RE_INITIALIZE="true"; fi
    done
}

function consul_template () {
    local src=$1
    local dst=$2
    consul-template -config ${WORKDIR}/consul.hcl -template "${src}:${dst}" -once
    [[ "$?" != "0" ]] && log_fatal "Failed to configure ${src}"
}

function conf_openvpn() {
    log_info "Configure OpenVPN certs"
    Certs='ca.crt dh2048.pem server.crt server.key'
    for cert in $Certs;
    do
        consul_template "${WORKDIR}/keys/${cert}.tpl" "${WORKDIR}/keys/${cert}"
    done

    log_info "Configure OpenVPN service"
    consul_template "${WORKDIR}/bin/openvpn.tpl" "${WORKDIR}/bin/openvpn.sh"
    chmod +x ${WORKDIR}/bin/openvpn.sh

    log_info "Configure OpenVPN client"
    consul_template "${WORKDIR}/client.ovpn.tpl" "${WORKDIR}/client.ovpn"

    log_info "Configure Login server"
    consul_template "${WORKDIR}/bin/login.py.tpl" "${WORKDIR}/bin/login.py"
    chmod +x ${WORKDIR}/bin/login.py
}

function run_init () {
    log_info "Enabling Vault secrets"
    vault_secrets_disable "${VAULT_KV_PATH}"
    vault_secrets_enable kv "${VAULT_KV_PATH}" "-version=2"

    log_info "Enabling Vault auth"
    # Don't loose defined already defined users
    # vault_auth_disable "${VAULT_KV_PATH}" || true
    vault_auth_enable userpass "${VAULT_KV_PATH}"

    log_info "Generating Certificates"
    sh "${WORKDIR}/scripts/generate_certs.sh"
    [[ "$?" != "0" ]] && log_fatal "Failed to initialize"
}

function initialize () {
    if [[ "$RE_INITIALIZE" == "true" ]]; then
        log_info "Generating OpenVPN Configuration"
        run_init
    else
        log_info "OpenVPN Configuration was already initialized"
    fi
}

function load_config() {
    cd $WORKDIR
    echo -e "Loading config from Vault"
    echo -e "      VAULT_ADDR:  ${VIVID}${VAULT_ADDR}${RST}"
    echo -e "      Config:      ${VIVID}${CONFIG}${RST}"
    echo -e "      Environment: ${VIVID}${VPN_ENV}${RST}"

    conf_iptables
    conf_openvpn
}