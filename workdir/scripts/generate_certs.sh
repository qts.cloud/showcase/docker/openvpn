#!/bin/ash

source "${WORKDIR}/scripts/functions.sh"


export IPTABLES='iptables -t nat -A POSTROUTING -o ${DEFAULT_GW} -j MASQUERADE
iptables -t nat -A POSTROUTING -o ${TUN} -j MASQUERADE
iptables -A INPUT -i ${DEFAULT_GW} -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -i ${TUN} -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -j ACCEPT'

# EASY_RSA CONFIG
EasyRSA_VER='3.0.7'
EasyRSA_FILE="EasyRSA-${EasyRSA_VER}"
TMP_WORKDIR="/tmp/EasyRSA"
EasyRSA_TGZ="${EasyRSA_FILE}.tgz"
EasyRSA_URL="https://github.com/OpenVPN/easy-rsa/releases/download/v${EasyRSA_VER}/${EasyRSA_TGZ}"
EasyRSA_VARS="${WORKDIR}/scripts/easyrsa_vars"


# Cleanup
[[ -d "${TMP_WORKDIR}" ]] && rm -rf "${TMP_WORKDIR}" || true
mkdir "${TMP_WORKDIR}"

# Download & Extract EasyRSA
log_info "Downloading ${EasyRSA_URL}"
wget --quiet ${EasyRSA_URL} && tar -C "${TMP_WORKDIR}" --strip-components=1 -zxf "${EasyRSA_TGZ}" && rm -f "${EasyRSA_TGZ}"
[[ "$?" != "0" ]] && log_fatal "Failed to download EasyRSA"

cp "${EasyRSA_VARS}" "${TMP_WORKDIR}/vars"
cd "${TMP_WORKDIR}"

# Initialise
log_info "Initialising..."
./easyrsa init-pki

# Build CA
log_info "Building CA..."
./easyrsa build-ca nopass

# Generate Server CSR
log_info "Generating Server CSR..."
./easyrsa gen-req server nopass

# Import & Sign Server CSR
log_info "Signing Server CSR..."
yes | ./easyrsa sign-req server server

# Generate DH
log_info "Generating Diffie-Hellman key exchange..."
./easyrsa gen-dh


# Save Certs
log_info "Uploading OpenVPN config to Vault"
vault kv put ${VAULT_PATH}/ca.crt value=@pki/ca.crt
vault kv put ${VAULT_PATH}/ca.key value=@pki/private/ca.key
vault kv put ${VAULT_PATH}/server.crt value=@pki/issued/server.crt
vault kv put ${VAULT_PATH}/server.key value=@pki/private/server.key
vault kv put ${VAULT_PATH}/server.csr value=@pki/reqs/server.req
vault kv put ${VAULT_PATH}/dh2048.pem value=@pki/dh.pem

# Save Server Config
vault kv put ${VAULT_PATH}/${CONFIG} cname="${VPN_CNAME}" dns="${VPN_DNS}" iptables="${IPTABLES}" network="${VPN_NETWORK}" port="${VPN_PORT}" protocol="${VPN_PROTOCOL}" public_ip="${PUBLIC_IP}" tun="${TUN}"
