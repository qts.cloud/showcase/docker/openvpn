FROM python:alpine

# set vault version
ARG VAULT_VERSION=1.5.0
ENV VAULT_VERSION=${VAULT_VERSION}

WORKDIR /openvpn

# Install requirements
RUN apk update && apk --no-cache add openvpn iptables curl wget openssl
RUN echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf

# Add files
ADD entrypoint.sh /
ADD workdir .

# Install Vault
RUN wget --quiet --output-document=/tmp/vault.zip https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip && \
    unzip /tmp/vault.zip -d bin/ && \
    rm -f /tmp/vault.zip
RUN chmod +x bin/consul-template bin/vault /entrypoint.sh
ENV PATH="PATH=$PATH:/openvpn/bin"

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 1195

CMD ["/bin/ash", "/entrypoint.sh"]