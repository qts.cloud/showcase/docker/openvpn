#!/bin/ash


### Env Variables
export WORKDIR="/openvpn"
export CONFIG=${CONFIG:-config}
export RE_INITIALIZE=${RE_INITIALIZE:-false}

# OVPN Config
export VPN_ENV="${VPN_ENV:-dev}"
export VPN_NETWORK="${VPN_NETWORK:-10.42.0.0/24}"
export VPN_PORT="${VPN_PORT:-1195}"
export VPN_PROTOCOL="${VPN_PROTOCOL:-udp}"
export VPN_DNS=${VPN_DNS:-8.8.8.8}
export VPN_CNAME="${VPN_CNAME:-undefined}"
export PUBLIC_IP="${PUBLIC_IP:-$(curl ifconfig.co)}"
export TUN="${TUN:-tun0}"

# Vault Config
export VAULT_ADDR=${VAULT_ADDR}
export VAULT_TOKEN=${VAULT_TOKEN}
export VAULT_SKIP_VERIFY=${VAULT_SKIP_VERIFY:-true}
export VAULT_KV_PATH=${VAULT_KV_PATH:-openvpn}
export VAULT_PATH="${VAULT_KV_PATH}/${VPN_ENV}"
export DEBUG="${DEBUG:-false}"

# Load functions
source "${WORKDIR}/scripts/functions.sh"
colorize

check_variables_defined VAULT_ADDR VAULT_TOKEN
check_vault_login
check_vault_key_exists "${VAULT_PATH}/${CONFIG}"
initialize
load_config

if [[ ${DEBUG} == "true" ]]; then 
    log_warn "Debug mode:"
    cat ${WORKDIR}/bin/iptables.sh
    cat ${WORKDIR}/bin/openvpn.sh
fi

log_info "Display generated client.ovpn"
cat ${WORKDIR}/client.ovpn

log_info "Starting OpenVPN"
ash ${WORKDIR}/bin/openvpn.sh
